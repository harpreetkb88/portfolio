import { Component, OnInit } from '@angular/core';
import { ContactService } from '../service/contact/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  name;
  email;
  phone;
  Message;

  constructor(private contact:ContactService) { }

  ngOnInit(): void {}
  
    addNotes(){
   this.contact.addNotes(this.name, this.email, this.phone, this.Message)
 }
}
