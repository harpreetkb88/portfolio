var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var Superhero = new Schema(
    {name : String,
    link : String, 
    type : String,
    phone : Number}
    );

// mongoose.model('Superheros', Superhero);


module.exports=mongoose.model('Superhero', Superhero);
