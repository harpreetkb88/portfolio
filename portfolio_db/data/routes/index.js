var express = require('express');
var router = express.Router();


require('../models/portfolio');
// var express = require('express');
// var router = express.Router();
var mongoose = require('mongoose');
var Portfolio = mongoose.model('Portfolio');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/add', function(req, res) {
  new Portfolio({name : req.body.name, email : req.body.email, phone : req.body.phone, Message : req.body.Message})
  .save(function(err, Portfolio) {
    // console.log(superhero);
    if(Portfolio){
    res.json({message:'Data Has Been Successfully Saved', success:true});
  }
  else
  res.json({message:err,success:false});
  });
});


//////////////////////////////////////////Update///////////////////////////////////////
router.post('/update/:id', function(req, res) {
  var query = {"_id": req.params.id};
  var update = {name : req.body.name, email : req.body.email, phone : req.body.phone, Message : req.body.Message};
  var options = {new: true};
  Portfolio.findOneAndUpdate(query, update, options, function(err,Portfolio){
    console.log(Portfolio)
    res.json(
     
      {"Value":"It Has been Updated"}
    );
  });
});


//////////////////////////////////////////Delete///////////////////////////////////////

router.post('/delete/:id', function(req, res) {
  var query = {"_id": req.params.id};
 Portfolio.findOneAndRemove(query, function(err, Portfolio){
    console.log(Portfolio)
    res.json({"Deleted":"Value Deleted"});
  });
});



// router.get('/hitme', function(req, res, next) {
//   res.render('about', { title: 'Express' });
// });


module.exports = router;
